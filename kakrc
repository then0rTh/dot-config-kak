add-highlighter global/ number-lines -separator ' '
add-highlighter global/ wrap
add-highlighter global/ show-matching
#add-highlighter global/ show-whitespaces

evaluate-commands %sh{
    if [ "$TERM_PROGRAM" = Apple_Terminal ]
    then
        echo colorscheme default;
        echo set-face global comment white+d;
        echo set-face global bracket cyan;
        echo set-face global comma cyan;
    else
        echo colorscheme one-dark;
    fi
}

set-option global tabstop 4
# hook window InsertChar \t %{ exec -draft -itersel h@ }
hook global InsertChar \t %{ try %{
    execute-keys -draft "h%opt{indentwidth}@"
    #execute-keys -draft "h<a-h><a-k>\A\h+\z<ret><a-;>;%opt{indentwidth}@"
}}
hook global InsertDelete ' ' %{ try %{
    execute-keys -draft 'h<a-h><a-k>\A\h+\z<ret>i<space><esc><lt>'
}}

# When the completion menu is visible in insert mode, Tab and Shift Tab will cycle through the available options.
# When the completion menu is not shown, or when the character before the cursor is whitespace,
# Tab and Shift Tab are just handled normally, so the previous tab-expansion example still works.
hook global InsertCompletionShow .* %{
    try %{
        # this command temporarily removes cursors preceded by whitespace;
        # if there are no cursors left, it raises an error, does not
        # continue to execute the mapping commands, and the error is eaten
        # by the `try` command so no warning appears.
        execute-keys -draft 'h<a-K>\h<ret>'
        map window insert <tab> <c-n>
        map window insert <s-tab> <c-p>
        hook -once -always window InsertCompletionHide .* %{
            unmap window insert <tab> <c-n>
            unmap window insert <s-tab> <c-p>
        }
    }
}



define-command elm %{
    write
    eval %sh{
        elm_make_report=$(elm make --report=json --output=/dev/null "$kak_buffile" 2>&1)
        elm_make_status=$?
        #echo "$elm_make_report" | tr -d '" ' | xargs echo echo
        #exit
        echo "$kak_buffile" | python3 "$kak_config/elm_report.py"
        if [ "$elm_make_status" = "0" ]
        then
            while [ ! -f elm.json ]
            do
                if basename `pwd` | python3 -c 'import sys; sys.exit(int(sys.stdin.read().strip()[0].isupper()))'
                then
                    cd ..
                else
                    break
                fi
            done
            if [ -f elm-compile.sh ]
            then
                sh elm-compile.sh 2>/dev/null >/dev/null
                elm_compile_status=$?
                if [ "$elm_compile_status" != "0" ]
                then
                    echo "echo This file is OK, but project build failed (elm-compile.sh status $elm_compile_status)"
                fi
            fi
        fi
    }
}


define-command run -params 1.. %{
    write
    info %sh{ $@ 2>&1 }
    #echo -debug %sh{ $@ }
    #buffer *debug*
}

define-command run-current -params 1.. %{
    write
    info %sh{ $@ "$kak_buffile" 2>&1 }
    #echo -debug %sh{ $@ }
    #buffer *debug*
}

hook global WinSetOption filetype=(clojure|lisp|scheme|racket) %{
        parinfer-enable-window -smart
}

