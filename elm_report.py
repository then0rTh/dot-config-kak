import subprocess
import json
import sys


def escape(val):
    if type(val) is not str:
        val = json.dumps(val)
    return '"' + val.replace('"', "'") + '"'

def md_escape(txt):
    return txt.replace(r'\{', r'\\{').replace(r'{', r'\{')


out, err = subprocess.Popen(
    ['elm', 'make', '--report=json', '--output=/dev/null', sys.stdin.read().strip()],
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE,
    text=True
).communicate()


if not err:
    print('echo', 'OK (no err)')
    sys.exit(0)

try:
    report = json.loads(err)
except Exception as e:
    print('echo', json.dumps("err: " + str(e)))
    sys.exit(1)
    report = dict(type = None)

if report['type'] == 'compile-errors':
    err = report['errors'][0]
    path = err['path']
    problem = err['problems'][-1]
    problem_start = problem['region']['start']
    print('edit', path, problem_start['line'], problem_start['column'])
    print('exec', 'vt', 'vk', 'vk', 'vk')

    STYLE_NORMAL = "{InformationText}"
    STYLE_ERROR = "{InformationError}"
    STYLE_INFO = "{Information}"
    def unwrap(msg_part):
        if isinstance(msg_part, dict):
            style = STYLE_ERROR if msg_part['color'] == 'RED' else STYLE_INFO
            return style + md_escape(msg_part['string']) + STYLE_NORMAL
        return md_escape(str(msg_part))
    msg = problem['message']
    msg = problem['title'] + '\n' + ''.join(map(unwrap, msg))
    
    print('info',
        '-markup',
        escape(STYLE_NORMAL + msg),
    )

else:
    print('echo', 'OK')
