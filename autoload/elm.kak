

hook global WinSetOption filetype=elm %{
    require-module elm
    add-highlighter shared/elm/code/ regex [-+<>!@#$%^&*=:/\\|.]                   0:operator
    add-highlighter shared/elm/code/ regex [\[\](){}]                              0:bracket
    add-highlighter shared/elm/code/ regex [,]                                     0:comma
    add-highlighter shared/elm/code/ regex \b(type|alias|if|then|else|case|of|let|in|infix)\b)     0:keyword
}
