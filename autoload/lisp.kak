

hook global WinSetOption filetype=lisp %{
    require-module lisp
    add-highlighter shared/lisp/code/ regex (?:\()[^\s)'`,]+ 0:function
    add-highlighter shared/lisp/code/ regex (?:\((def[a-z]+|lambda)\s*[^()]*\()([^()]+|[(][^()]+[)])+ 0:variable
    add-highlighter shared/lisp/code/ regex ('|#'|`|,|,@|&body|&optional|&key|&rest) 0:operator
    add-highlighter shared/lisp/code/ regex (?:[^\w-]):[\w-]+ 0:operator
    add-highlighter shared/lisp/code/ regex [\w-]+:+ 0:comment
    add-highlighter shared/lisp/code/ regex (?:\(def[a-z]+\s+)[^\s()]+ 0:function
    add-highlighter shared/lisp/code/ regex (?:\()(def[a-z]+|if|do|let\*?|labels|lambda|catch|and|or|assert|while|def|do|fn|finally|let|loop|new|quote|recur|set!|throw|try|var|case|if-let|if-not|when|when-first|when-let|when-not|(cond(->|->>)?))(?:\s|\(|\)|$) 0:keyword

    add-highlighter shared/lisp/code/ regex [()] 0:bracket
}
