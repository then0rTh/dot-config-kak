

hook global WinSetOption filetype=python %{
    require-module python
    add-highlighter shared/python/code/ regex (->|:|\.|,|<=|<<|>>|>=|<>?|>|!=|==|\||\^|&|\+|-|\*\*?|//?|%|~) 0:operator
    add-highlighter shared/python/code/ regex ((?<![=<>!]):?=(?![=])|[+*-]=|[\[\]()]) 0:operator
}

