

hook global WinSetOption filetype=(c|cpp|objc) %{
    require-module c-family
    add-highlighter shared/c/code/brackets regex [\[\](){}] 0:bracket
    add-highlighter shared/c/code/separators regex [,\;] 0:comma
    add-highlighter shared/c/code/operators regex [!<>=|&*/+.?:-] 0:operator
}
